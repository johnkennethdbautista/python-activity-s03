class Camper():
	def __init__(self, name, batch, course_type):
		self.name = name
		self.batch = batch
		self.course_type = course_type

	# methods
	def career_track(self):
		print(f"Currently enrolled in the {self.course_type}")

	# Mini-Exercise Solution
	def info(self):
		print(f"My name is {self.name} of batch {self.batch}")
camper_name = str(input("Camper name: "))
camper_batch = str(input("Camper batch: "))
camper_course = str(input("Camper course: "))
zuitt_camper = Camper(camper_name, camper_batch, camper_course)

zuitt_camper.info()
zuitt_camper.career_track()
